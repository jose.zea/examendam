import React, {Component} from 'react';
import {View, FlatList, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { Searchbar, ActivityIndicator } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import firebase from '../firebase';

function Item({ Titulo,informacion,longitud,latitud,imagen,navigation,platos,direccion }) {
  return (
      <View style= {styles.container}>
      <View style={{flex:1, flexDirection: 'row'}}>
          <Image style={styles.imageView} source = {{uri: imagen}}/>
          <View style= {styles.textView}>
              <Text style= {styles.title}>{Titulo}</Text>
          </View>
          <View style={styles.textvolver}>
            <TouchableOpacity 
              onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    navigation.navigate('Informacion', {
                        Titulo: Titulo, informacion: informacion,
                        longitud: longitud, latitud:latitud,
                        imagen: imagen, direccion: direccion,
                        platos: platos,
                    });
                    }}>
              <MaterialCommunityIcons name="arrow-right" size={26} />
            </TouchableOpacity >
          </View>
      </View>
      </View>
  );
}
  

export default class ListaRestaurant extends Component{
    
    constructor(props){
        super(props);
        this.state = {
          text: "",
          items: [],
          data: [],
      };        
    }

    async componentDidMount() {
      let data = []

      const DATA = [
        {
          id: '1',
          titulo: 'Nueva palomino',
          informacion:"Preparamos cominda criolla 100% arequipeña",
          longitud: "-71.539586",
          latitud:"-16.3917321",
          imagen:'https://i.pinimg.com/originals/d2/54/41/d254414586ad59656f4ebdb99fa7b5e8.jpg',
          platos:[{plato_nombre: 'Adobo', precio: '35'},{plato_nombre: 'Picante', precio: '45'}]
        },
        {
          id: '2',
          titulo: 'Pizzeria Dominos',
          informacion:"Solo dinero en efectivo · Delivery a partir desde las 5pm",
          longitud: "-71.539586",
          latitud:"-16.3917321",
          imagen:'https://i.pinimg.com/originals/0e/1f/53/0e1f53ae8cdfe8ccae1692dc8b1056aa.jpg',
          platos:[{plato_nombre: 'Americana', precio: '30'},{plato_nombre: 'Triple carne', precio: '55'}]
        },
        {
          id: '3',
          titulo: 'Los pollos hermanos',
          informacion:"Polleria con opcion a delivery, atendemos desde las 11 am.",
          longitud: "-71.539586",
          latitud:"-16.3917321",
          imagen:'https://i.pinimg.com/originals/b6/a3/5e/b6a35eb59793d73bf721b5515ae964f6.jpg',
          platos:[{plato_nombre: 'Cuarto de pollo', precio: '16'},{plato_nombre: 'Pollo entero', precio: '55'}]
        },
        
      ];
      this.setState({ data: DATA });
      this.setState({ items: DATA });
  }

    renderSeparatorView = () => {
      return (
        <View style={{
            height: 1, 
            width: "100%",
            backgroundColor: "#1C0F13",
          }}
        />
      );
    };

  
 

    render(){
        
        return(
            <View style={styles.list}>
            <FlatList style={styles.container}
                data={this.state.data}
                renderItem={({ item }) => <Item platos={item.platos}
                  Titulo={item.titulo} informacion={item.informacion}
                  latitud={item.latitud} longitud={item.longitud} direccion={item.direccion}
                  navigation={this.props.navigation} imagen={item.imagen}/>}
                keyExtractor={item => item.id}
                ListEmptyComponent={() => (<ActivityIndicator style={{marginTop: 25}} animating={true} color='#666A86' />)}
                ListHeaderComponent={this.renderHeader}
                ItemSeparatorComponent={this.renderSeparatorView}
            />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        padding:50,
        justifyContent: 'center',
        alignContent: 'center',
      },
    list: {
      flex: 1,
      backgroundColor: '#E8DDB5',
    },
    container: {
      marginTop: 15,
      margin: 20,
      backgroundColor: '#E8DDB5'
    },
    imageView: {
        width: '40%',
        height: 80 ,
        margin: 5,
        borderRadius: 5, 
    },
    textView: {
        width:'40%', 
        textAlignVertical:'center',
        textAlign:'justify',
        justifyContent: 'center',
        fontFamily: 'Pacifico-Regular',
        margin: 10,
    },
    textvolver:{
      marginRight: 20,
      justifyContent: 'center'
    },
    item: {
      backgroundColor: 'gray',
      padding: 10
    },
    title: {
      fontSize: 20,
      color: '#000000',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
    titleloading: {
        fontSize: 40,
        color: '#394053',
        fontWeight: 'bold',
        fontFamily: 'Bangers',
      },
  });