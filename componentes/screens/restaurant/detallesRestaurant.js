import React, {Component} from 'react';
import {View, FlatList, StyleSheet, ScrollView, Text, Image,TouchableOpacity  } from 'react-native';
import { IconButton, Colors, Avatar } from 'react-native-paper';

function Item({ nombre,precio,navigation }) {
    return (
        <View style= {styles.card}>
            <View style= {styles.cardHeader}>
                <Text style= {styles.title}>{nombre}</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent: 'center', alignContent: 'center', alignItems:'center'}}>
                <Text style={{fontSize:20, fontWeight: 'bold', color:"#666A86"}}>S/. {precio}</Text>
                <IconButton
                icon="cart"
                color={Colors.redA700}
                size={35}
                onPress={() => {
                  navigation.navigate('TransferenciaUno', {
                      nombre:nombre,
                      precio:precio
                  });
                }}
              />
            </View>
        </View>
    );
  }

export default class DetalleRestaurant extends Component{
    
    constructor(props){
        super(props);
        this.state = {
  
      };        
    }
    render(){
      const { Titulo } = this.props.route.params;
      const { informacion } = this.props.route.params;
      const { longitud } = this.props.route.params;
      const { latitud } = this.props.route.params;
      const { imagen } = this.props.route.params;
      const { platos } = this.props.route.params;
      const { direccion } = this.props.route.params;
      return(
        <ScrollView style={{backgroundColor:"#E8DDB5"}}>
          <View style={styles.container}>
            <Text style={styles.text}>{Titulo}</Text>
            <Image source = {{uri: imagen}} style={{height: 300, width: 300, borderRadius: 20}}/>
            <Text style={styles.textView}>{informacion}</Text>
            <Text style={styles.textView}>Dirección: {direccion}</Text>
            <View style={styles.button} >
                <TouchableOpacity onPress={() => {
                        /* 1. Navigate to the Details route with params */
                        this.props.navigation.navigate('Mapa', {
                            Titulo:Titulo,informacion:informacion,
                            longitud:longitud, latitud:latitud,
                            imagen:imagen
                        });
                }}>
              <View style={{flexDirection: 'row'}}>
                  <Avatar.Icon size={38} style={{backgroundColor: 'transparent', alignSelf: 'center'}} icon="map-marker" />
                <Text style={styles.textButon}>Como Llegar</Text>
              </View>
            </TouchableOpacity >
            </View>
            <FlatList
                data={platos}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                numColumns={1}
                renderItem={({ item }) => <Item nombre={item.plato_nombre} precio={item.precio} navigation={this.props.navigation}/>}
            />
        </View>
      </ScrollView>
    )      
    }
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
     
    },
    mainButton: {
      marginRight: 15,
    },
    textSub: {
        width:'70%', 
        textAlignVertical:'center',
        color: '#f4511e',
        textAlign:'justify',
        fontSize: 40,
    },
    textTitle:{
        fontWeight: 'bold',
    },
    textView: {
        textAlign: 'center',
        color: '#394053',
        fontSize: 18,
        padding: 10
    },
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      color: '#666A86',
      paddingHorizontal:40,
      paddingVertical: 10,
      fontFamily: "Pacifico-Regular",
    },
    button: {
      backgroundColor: '#666A86',
      alignItems: "center",
      alignContent: 'center',
      justifyContent: "space-around",
      padding: 5,
      width:190,
      borderRadius: 10,
      margin:10,
    },
    countContainer: {
      alignItems: 'center',
      padding: 10,
    },
    countText: {
      color: '#FF00FF',
    },
    textButon: {
        alignItems: 'center',
        padding: 5,
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
      },
      card:{
        width: 150,
        shadowColor: '#00000021',
        shadowOffset: {
          width: 0,
          height: 6,
        },
        justifyContent: 'space-between',
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
        marginVertical: 10,
        backgroundColor:"#ffe1ad",
        flexBasis: '42%',
        marginHorizontal: 10,
      },
      cardHeader: {
        paddingVertical: 10,
        paddingHorizontal: 16,
        borderTopLeftRadius: 1,
        borderTopRightRadius: 1,
        flexDirection: 'row',
        alignItems:"center", 
        justifyContent:"center"
      },
      title:{
        fontSize:20,
        fontWeight: 'bold',
        flex:1,
        alignSelf:'center',
        color:"#666A86"
      },
  })