var moment = require('moment'); //yarn add moment 
moment.locale('os');

import React, {Component} from 'react';
import {Text, View, StyleSheet,ScrollView} from 'react-native';

import DateTimePickerModal from 'react-native-modal-datetime-picker';  // yarn add react-native-modal-datetime-picker @react-native-community/datetimepicker

import { TextInput, Button, RadioButton, Caption, Title, Paragraph, Switch, HelperText } from 'react-native-paper';

export default class TransferenciaUno extends Component{
    
    constructor(props){
        super(props);
        let now = moment().format("DD/MM/YYYY");

        this.state={
            isDatePickerVisible: false,
            direccionDestino:'',
            numeroContacto:'',
            fecha: now,
            radioButtonMetodoPago:'',
            unidades:0,
            mensajeerrorunidades:"",
            mensajerrorcontacto:"",
            mensajerrorcuenta:"",
            mensajerrordireccion:"",
            mensajeerrortipopago:"",
            mensajeerrortipopago:"",
        };
    }
 
    changeDireccionEnvio = direccionDestino =>{
        this.setState({direccionDestino,mensajerrordireccion:""});
    };
    changeNumeroContacto = numeroContacto =>{
        var re = /^\d{9}$/;
        if(re.test(numeroContacto)) this.setState({numeroContacto:numeroContacto,mensajerrorcontacto:''})
        else this.setState({numeroContacto:numeroContacto,mensajerrorcontacto: 'Debe ingresar un numero valido'})
    };
    //-----------------------------MANEJO DE ERRORES DEL HELP TEXT----------------//
    /*_hasErrors = () => {
        return this.state.direccionDestino.length==0
    }
    _numberHasErrors = () => {
        return this.state.numeroContacto.length==0
    }*/
    changeUnidades = unidades =>{
        var re = /^\d+(\.\d{1,2})?$/;
        if(re.test(unidades)) this.setState({unidades:unidades,mensajeerrorunidades: ''})
        else this.setState({unidades:unidades,mensajeerrorunidades: 'Este campo debe tener solo números'})
    };
    //----------------------------------------------------------------------------//
    async validacion(nombre,precio,total){
        await this.validator();
        if(this.state.mensajeerrortipopago==""&&this.state.mensajeerrorunidades==""&&this.state.mensajerrorcontacto==""&&this.state.mensajerrorcuenta==""&&this.state.mensajerrordireccion==""){
            this.props.navigation.navigate('TransferenciaDos' ,{
                direccion: this.state.direccionDestino,
                numero: this.state.numeroContacto,
                fecha:this.state.fecha,
                metodoPago:this.state.radioButtonMetodoPago,
                unidades: this.state.unidades,
                nombre:nombre,
                precio:precio,
                total:total
                })
      }
    }
    validator () {
        if(this.state.direccionDestino===""){
          this.setState({mensajerrordireccion:"Debe indicar una direccion"})
        }
        if(this.state.numeroContacto===""){
          this.setState({mensajerrorcontacto:"Debe indicar un numero telefonico"})
        }
        if(this.state.unidades==0){
          this.setState({mensajeerrorunidades:"Debe ingresar un numero de platos"})
        }
        if(this.state.radioButtonMetodoPago===""){
            this.setState({mensajeerrortipopago:"Debe ingresar un metodo de pago"})
          }
    }
    render(){

        const { nombre } = this.props.route.params;
        const { precio } = this.props.route.params;
        const total = this.state.unidades*this.props.route.params.precio || 0;
        //--------FUNCIONES DEL DATEPICKER------------------------//
        const showDatePicker = () => {
            this.setState({isDatePickerVisible:true});
        };
         
        const hideDatePicker = () => {
            this.setState({isDatePickerVisible:false});
        };
         
        const handleConfirm = (date) => {
            let fecNew = moment(date).format("DD/MM/YYYY");
        this.setState({fecha: fecNew, isDatePickerVisible:false});
        };
        //---------------------------------------------------------//
        
        return(
            
            <ScrollView style={{backgroundColor:"#E8DDB5"}}>
            <View style={{ margin:18, justifyContent: 'center'}}>
                <Title style={{alignSelf:'center', marginBottom:10, fontFamily:'sans-serif-medium' , fontStyle:'italic'}}>Registro de pedido</Title>
                <Paragraph>Direccion de envio</Paragraph>
                <TextInput
                    style={{backgroundColor:"#ffe1ad"}}
                    placeholder='Ingrese su direccion.'
                    mode='outlined'
                    underlineColorAndroid='#C75483' // no reconoce cambio de color
                    selectionColor='#C75483'        //no reconoce cambio
                    onChangeText={direccion => this.changeDireccionEnvio(direccion)}
                />
                { this.state.mensajerrororigen != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajerrordireccion}</Text>:<View></View>}
                    

                <Paragraph>Numero de contacto</Paragraph>
                <TextInput
                    placeholder='Ingrese su telefono'
                    mode='outlined'
                    style={{backgroundColor:"#ffe1ad"}}
                    onChangeText={numeroContacto => this.changeNumeroContacto(numeroContacto)}
                />
                 { this.state.mensajerrororigen != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajerrorcontacto}</Text>:<View></View>}                 
              
                <Paragraph>Plato Escogido</Paragraph>
                    <TextInput
                        
                        style={{backgroundColor:"#ffe1ad"}}
                        placeholder=''
                        mode='outlined'
                        value={nombre}
                        disabled
                    />
                <View>
                <Paragraph style={{marginTop:25}}>Cantidad de unidades a comprar y total:</Paragraph>
                <View style={{ flexDirection:"row", alignSelf:'center',alignItems:'center', justifyContent:'center'}}>
                    <TextInput 
                        style={{ width:'30%',backgroundColor:"#ffe1ad" }}
                        placeholder='Unidades'
                        mode='outlined'
                        onChangeText={numero => this.changeUnidades(numero)}
                    />
                    <TextInput
                        style={{flex:1,backgroundColor:"#ffe1ad", alignSelf:'center',marginLeft:20,width:'70%' }}
                        disabled
                        placeholder=''
                        mode='outlined'
                    >S/. {total} Nuevos soles.</TextInput>
                </View>
                { this.state.mensajerrororigen != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajeerrorunidades}</Text>:<View></View>}
                   
                <Paragraph style={{marginTop:5, alignSelf:'center'}}>Elija la fecha de entrega:</Paragraph>
                    <Button style={{marginTop:5, width:150,alignSelf:'center'}} 
                     mode='contained' color='#666A86' onPress={showDatePicker}>
                        {this.state.fecha}
                    </Button>
                    <DateTimePickerModal
                        isVisible={this.state.isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />
                </View>

                <View style={{alignSelf:'center', marginLeft:50, marginRight:50, marginTop:10}}>
                    <RadioButton.Group
                    onValueChange={radioButtonMetodoPago => this.setState({ radioButtonMetodoPago ,mensajeerrortipopago:""})}
                    value={this.state.radioButtonMetodoPago}
                    >
                    <View  style={{flexDirection:'row', justifyContent:'center'}}>
                        <RadioButton value="Cuenta bancaria" color='#666A86' />
                        <Caption style={{alignSelf:'center'}}>Pagar por cuenta</Caption>
                        <RadioButton value="Pago efectivo" color='#666A86' />
                        <Caption style={{alignSelf:'center'}}>Pago en llegada</Caption>
                    </View>
                    </RadioButton.Group>
                    { this.state.mensajerrororigen != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajeerrortipopago}</Text>:<View></View>}
                </View>

                <Button style={{marginTop:5, width:150,alignSelf:'center'}} 
                    color='#666A86'
                    icon="cart" mode="contained" onPress={() => this.validacion(nombre,precio,total)}>
                    Confirmar
                </Button>
            
            </View>
            </ScrollView>
        )
    }

}
const styles = StyleSheet.create({
    texterror:{
      color:'#b22222'
    },
  })
