import React, {Component} from 'react';
import {Text, View, StyleSheet,Alert,ScrollView} from 'react-native';
import { TextInput, Button, Caption, Title, Paragraph, Card} from 'react-native-paper';
import firebase from '../firebase'

export default class TransferenciaDos extends Component{
    constructor(props){
        super(props)
        this.state={
            direccion: '',
            celular: '',
            numero:'',
            fecha:'',
            metodoPago:'',
        };
    } 
    showAlert = () =>{
        Alert.alert(
          'Confirmacion de Envio',
          '¿Esta seguro que los datos son correctos?',
          [
            {
              text:'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style:'cancel',
            },
            {text:'OK', onPress: () => {
                let userId = firebase.auth().currentUser.uid;
                
                firebase.database().ref('/usuarios/' + userId + '/pedidos/').push({
                    fecha: this.props.route.params.fecha,
                    pedido: `${this.props.route.params.unidades} ${this.props.route.params.nombre}`,
                    total: this.props.route.params.total
                });
                console.log("Usuario: "+userId+" registro su pedido");
                this.props.navigation.navigate('TransferenciaTres')
            }}
          ],
          {cancelable: false},
        );
      };

    render(){
        const { direccion, numero, fecha, metodoPago, plato} = this.props.route.params
        const { nombre } = this.props.route.params;
        const { unidades } = this.props.route.params;
        const { precio } = this.props.route.params;
        const { total } = this.props.route.params;
        return(
            <ScrollView style={{backgroundColor:"#E8DDB5"}}>
            <View style={{ margin:18, justifyContent: 'center', backgroundColor:"#ffe1ad"}}>
 
                <Card style={{backgroundColor:"#ffe1ad"}}>
                    <Card.Title title="Resumen de informacion" subtitle=""/>
                    <Card.Content>
                        <Paragraph style={{fontStyle:'italic', fontFamily:'sans-serif-medium'}}>Direccion a enviar:</Paragraph>
                        <Caption>{direccion}</Caption>
                        <Paragraph style={{fontStyle:'italic', fontFamily:'sans-serif-medium'}}>Numero de contacto:</Paragraph>
                        <Caption>{numero}</Caption>
                        <Paragraph style={{fontStyle:'italic', fontFamily:'sans-serif-medium'}}>Metodo de pago</Paragraph>
                        <Caption>{metodoPago}</Caption>
                        <Paragraph style={{fontStyle:'italic', fontFamily:'sans-serif-medium'}}>Fecha de entrega</Paragraph>
                        <Caption>La fecha de entrega es el {fecha}</Caption>
                        <Title>Items del pedido</Title>
                        <Paragraph style={{fontStyle:'italic', fontFamily:'sans-serif-medium'}}>Plato(s) predidos:</Paragraph>
                        <Caption>{unidades} {nombre}</Caption>
                        <Paragraph style={{fontStyle:'italic', fontFamily:'sans-serif-medium'}}>Precio Total:</Paragraph>
                        <Caption>S/. {total}</Caption>

                    </Card.Content>
                    <Card.Cover style={{marginTop:10}} source={{ uri: 'https://desarrollodeaplicaciones.org/wp-content/uploads/2019/04/dibujo-de-motorizado-haciendo-delivery.png' }} />

                </Card>

                <View style={{ flexDirection:"row", alignContent:'space-around', justifyContent:'center', padding:10 }}>

                    <Button style={{marginTop:10, borderWidth:2, marginHorizontal:8 }}
                        icon="arrow-left" color='#666A86' mode="outlined" onPress={() => {this.props.navigation.goBack()}}>
                        Volver
                    </Button>
                    <Button style={{marginTop:10, marginHorizontal:8 }} icon="check-bold" color='#666A86' mode="contained" onPress={()=>this.showAlert()}>
                        Aceptar
                    </Button>

                </View>

            </View>
            </ScrollView>
        )
    }


}

