import React, {Component} from 'react';
import {View, FlatList, StyleSheet, ScrollView, Text, Image,Dimensions,TouchableOpacity  } from 'react-native';

var { height } = Dimensions.get('window');
export default class PedidoRestaurant extends Component{
    
    constructor(props){
        super(props);
        this.state = {
  
      };        
    }
    render(){
      const { nombre } = this.props.route.params;
      const { precio } = this.props.route.params;
      
      return(
        <ScrollView >
          <View style={styles.container}>
            <Text style={styles.text}>{nombre}</Text>
            <Text style={styles.text}>{precio}</Text>
        </View>
      </ScrollView>
    )      
    }
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
     
    },
    mainButton: {
      marginRight: 15,
    },
    textSub: {
        width:'70%', 
        textAlignVertical:'center',
        color: '#f4511e',
        textAlign:'justify',
        fontSize: 40,
    },
    textTitle:{
        fontWeight: 'bold',
    },
    textView: {
        textAlign: 'center',
        color: '#394053',
        fontSize: 16,
        padding: 10
    },
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      color: '#666A86',
      paddingHorizontal:40,
      paddingVertical: 10,
      fontFamily: "Pacifico-Regular",
    },
    button: {
      backgroundColor: '#666A86',
      alignItems: "center",
      justifyContent: "space-around",
      padding: 5,
      width:150,
      borderRadius: 10,
      margin:10,
    },
    countContainer: {
      alignItems: 'center',
      padding: 10,
    },
    countText: {
      color: '#FF00FF',
    },
    textButon: {
        alignItems: 'center',
        padding: 5,
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
      },
  })