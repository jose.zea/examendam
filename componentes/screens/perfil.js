import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { List, Avatar, Colors, Modal, Portal, Text, Button, Provider  } from 'react-native-paper';
import firebase from './firebase';

function Home(props) {
  const { navigation } = props
  const user = firebase.auth().currentUser
  const [visible, setVisible] = React.useState(false)

  function onSignOut(){
    firebase.auth().signOut()
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }],
    });
  }

  return (
        <ScrollView style={{ backgroundColor: '#E8DDB5'}}>
          <View style={styles.container}>
            <View style={styles.menuPerfil}>
                <List.Item
                    title={user.email}
                    titleStyle={{ color:Colors.redA700, fontWeight:'bold', fontSize:20 }}
                    />
                <List.Item
                  title="Mi cuenta"
                  left={props =>  <List.Icon color={Colors.redA700} icon="account" />}
                  right={props =>  <List.Icon color={Colors.redA700} icon="chevron-right" />}
                />
                <List.Item
                  title="Dirección"
                  left={props =>  <List.Icon color={Colors.redA700} icon="eye" />}
                  right={props =>  <List.Icon color={Colors.redA700} icon="chevron-right" />}
                />
                <List.Item
                  title="Configuración"
                  left={props =>  <List.Icon color={Colors.redA700} icon="security" />}
                  right={props =>  <List.Icon color={Colors.redA700} icon="chevron-right" />}
                />
                <List.Item
                  title="Acerca de esta app"
                  left={props =>  <List.Icon color={Colors.redA700} icon="help-circle" />}
                  right={props =>  <List.Icon color={Colors.redA700} icon="chevron-right" />}
                />
                <List.Item
                  title="Cerrar sesión"
                  onPress={onSignOut}
                  left={props =>  <List.Icon color={Colors.redA700} icon="exit-to-app" />}
                  right={props =>  <List.Icon color={Colors.redA700} icon="chevron-right" />}
                />
            </View>
          </View>
      </ScrollView> 
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  menuPerfil:{
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 20,
    backgroundColor:"#ffe1ad",
    flexBasis: '42%',
    marginHorizontal: 20,
  },
})

export default Home