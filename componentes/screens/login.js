import React from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView } from 'react-native';
import { TextInput , Button } from 'react-native-paper';
import firebase from './firebase';
import Loader from './loader';

function Login(props) {
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [error, setError] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const { navigation } = props;

    async function onLogin(){
        setLoading(true);
        await firebase.auth().signInWithEmailAndPassword(username, password).then( async() => {
          let userId = firebase.auth().currentUser.uid;
          console.log("Usuario: "+userId+" se ha logeado");
          setError('');
          setLoading(false);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Home' }],
          });
        })
        .catch(error => {
          console.log(error.toString());
          setLoading(false);
          setError('Usuario o contraseña incorrectos.');
        });
    }

    return ( 
          <View style={styles.container}>
            <ImageBackground source={require('../assets/img/fastFoodBG1.png')} resizeMode='cover' style={ styles.imgBackground } >
              <Loader loading={loading} />
              <View style={styles.login}>
              <ScrollView>
                <Text style={styles.logo}>Delivery APP</Text>
                <TextInput
                  label='Email'
                  value={username}
                  mode='outlined'
                  keyboardType='email-address'
                  style={styles.inputText}
                  theme={{ colors: { primary: '#666A86',underlineColor:'transparent',}}}
                  onChangeText={setUsername}
                />
                <TextInput
                  label='Contraseña'
                  value={password}
                  mode='outlined'
                  secureTextEntry={true}
                  style={styles.inputText}
                  theme={{ colors: { primary: '#666A86',underlineColor:'transparent',}}}
                  onChangeText={setPassword}
                />
                <Text style={{alignSelf:'center', fontSize: 12, color: '#666A86'}}>{error}</Text>
                <Button mode='text' color='#666A86' labelStyle={{fontSize: 12}}>¿Olvido su contrseña?</Button>
                <Button
                  mode="contained"
                  dark={true}
                  style={styles.btnLogin}
                  color='#666A86'
                  onPress={onLogin}
                >INGRESAR</Button>
                <Button
                  mode="text"
                  style={styles.btnReg}
                  labelStyle={styles.txtReg}
                  onPress={() => navigation.navigate('Registrarse')}
                >REGISTRARSE</Button>
              </ScrollView>
              </View>
            </ImageBackground>
          </View>
    )}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imgBackground: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "flex-end"
  },
  login:{
    justifyContent: 'flex-end',
    backgroundColor: '#ffe4b5',
    shadowColor: '#00000021',

    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 15,
    borderRadius: 15,
    marginHorizontal: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  logo:{
    fontWeight:"bold",
    fontSize:35,
    color:"#666A86",
    marginBottom:10,
  },
  inputText: {
    backgroundColor: "#ffe4b5",
    marginHorizontal: 10,
    marginBottom: 5
  },
  btnLogin: {
    marginHorizontal: 30,
    marginTop: 7
  },
  btnReg: {
    marginHorizontal: 30,
    marginVertical: 5
  },
  txtReg: {
    color: '#666A86'
  }
});

export default Login