import firebase from 'firebase';


const firebaseConfig = {
    apiKey: "AIzaSyCPjmZzgYn2YaBEgAaHz462kdwuHfkO898",
    authDomain: "pc2dam-de3e3.firebaseapp.com",
    databaseURL: "https://pc2dam-de3e3.firebaseio.com",
    projectId: "pc2dam-de3e3",
    storageBucket: "pc2dam-de3e3.appspot.com",
    messagingSenderId: "790138159225",
    appId: "1:790138159225:web:0b070d53dcca4ff9d8d4a8",
    measurementId: "G-RMB83HB7MY"
  };
 

firebase.initializeApp(firebaseConfig);
export default firebase;