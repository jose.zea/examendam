import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, FlatList, SafeAreaView } from 'react-native';
import { Avatar, Badge, Image, ActivityIndicator, Card, Title, Paragraph,Button } from 'react-native-paper';
import firebase from './firebase';
/*pilco XD estubo  AQUI*/
function Home(props) {
  const { navigation } = props
  const [restaurant, setRestaurant] = useState([]);


  const food = [
    { id: 1, title: "Pizza", image: require('../assets/img/icons/pizza.png') },
    { id: 2, title: "Menu", image: require('../assets/img/icons/menu.png') },
    { id: 3, title: "Parrilla", image: require('../assets/img/icons/carne.png') },
    { id: 4, title: "Mar", image: require('../assets/img/icons/ceviche.png') },
    { id: 5, title: "Picanteria", image: require('../assets/img/icons/chile.png') },
    { id: 6, title: "Vegetariana", image: require('../assets/img/icons/vegetariano.png') },
  ]

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Tipos de comidas</Text>
      <SafeAreaView>
        <FlatList style={{ padding: 5 }}
          data={food}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          legacyImplementation={false}
          numColumns={1}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity style={styles.iconCard} >
                <Avatar.Image style={{ backgroundColor: 'transparent' }} size={50} source={item.image} />
                <View>
                  <View >
                    <Text style={{ fontSize: 10 }}>{item.title}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )
          }} />
      </SafeAreaView>
      
      <Card style={{margin:20}}>
        <Card.Content style={{paddingBottom:20, backgroundColor:'#E0D0C1'}}>
          <Title>Bienvenido</Title>
          <Paragraph>En la lista de restaurantes podra ubicar los diferentes restaurantes de la region de Arequipa</Paragraph>
        </Card.Content>
        <Card.Cover source={{ uri: 'https://peru21.pe/resizer/0tEdeK4w0noM__SoRUP_IdCq9o0=/980x528/smart/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/NXIQ6GVCL5FQVLWDXOIBG5LCPI.jpg' }} />

      </Card>

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1, backgroundColor: '#E8DDB5'
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 20,
    color: "#666A86"
  },
  list: {
    paddingHorizontal: 5
  },
  listContainer: {
    alignItems: 'center'
  },
  /******** card **************/
  iconCard: {
    backgroundColor: "#ffe4b5",
    alignItems: "center",
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    marginVertical: 2,
    shadowRadius: 7.49,
    elevation: 12,
    marginHorizontal: 5,
    justifyContent: "center",
    height: 85, width: 80
  },
  card: {
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    justifyContent: 'space-between',
    elevation: 12,
    marginVertical: 10,
    backgroundColor: "#ffe1ad",
    flexBasis: '42%',
    marginHorizontal: 10,
  },
  cardHeader: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "center"
  },
  cardContent: {
    paddingHorizontal: 16,
    paddingBottom: 8
  },
  cardFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 8,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  cardImage: {
    alignSelf: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex: 1,
    alignSelf: 'center',
    color: "#666A86"
  },
})

export default Home