import React, {useEffect} from 'react'
import { StyleSheet, View, Text, FlatList } from 'react-native'
import { List, Avatar, ActivityIndicator} from 'react-native-paper'
import firebase from './firebase'

function Home(props) {
  const { navigation } = props
  const [pedidos, setPedidos] = React.useState([])

  useEffect(() => { 
    getResponse();
  }, []);

  const getResponse = async () => {
    let data = []
    let userId = firebase.auth().currentUser.uid;
    await firebase.database().ref('/usuarios/'+userId+'/pedidos')
    .once('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childData = childSnapshot.val();
        data.push(childData)
      });
    });
    setPedidos(data)
    console.log(pedidos)
  };

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Mis pedidos</Text>
      <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={pedidos}
          horizontal={false}
          ListEmptyComponent={() => (<ActivityIndicator style={{marginTop: 25}} animating={true} color='#666A86' />)}
          renderItem={({item}) => {
            return (
              <List.Item
                title={item.pedido}
                description={`Costo: S/. ${item.total} \nFecha de entrega: ${item.fecha}`}
                left={props => <Avatar.Image size={80} source={require('../assets/img/icons/pedido.png')} />}
            />
            )
          }}/>
      
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1, backgroundColor: '#E8DDB5'
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 20,
    paddingVertical: 5,
    color:"#666A86"
  }
})

export default Home