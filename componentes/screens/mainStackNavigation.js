import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import Login from './login';
import Home from './home';
import Order from './order';
import Perfil from './perfil';
import Registrarse from './registrarse';
import ListaRestaurant from './restaurant/ListaRestaurant';
import DetalleRestaurant from './restaurant/detallesRestaurant';
import MapRestaurant from './restaurant/MapRestaurant';
import TopNavigation from './TopNavigation'
import TransferenciaUno from './restaurant/transferenciaOne'
import TransferenciaDos  from './restaurant/transferenciaDos'
import TransferenciaTres from './restaurant/trasnferenciaTres'

const Stack = createStackNavigator();
const stackrestaurant = createStackNavigator();
function RestaurantStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Restaurantes" component={ListaRestaurant} 
      options={({navigation}) => (
        { 
        title: 'Restaurantes',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#666A86',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="Informacion" component={DetalleRestaurant} 
      options={({navigation}) => (
        { 
        title: 'Informacion',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#666A86',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="Mapa" component={MapRestaurant} 
      options={({navigation}) => (
        { 
        title: 'Ubicacion',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#394053',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="TransferenciaUno" component={TransferenciaUno} 
      options={({navigation}) => (
        { 
        title: 'Pedido',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#394053',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="TransferenciaDos" component={TransferenciaDos} 
      options={({navigation}) => (
        { 
        title: 'Pedido',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#666A86',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="TransferenciaTres" component={TransferenciaTres} 
      options={({navigation}) => (
        { 
        title: 'Pedido',headerLeft: null,
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#666A86',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}
function HomeStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Home" component={Home} 
      options={({navigation}) => (
        { 
        title: 'Home',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#666A86',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}
function OrdernStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Ordenes" component={Order} 
      options={({navigation}) => (
        { 
        title: 'Ordenes',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#666A86',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}
function perfilStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Perfil" component={Perfil} 
      options={({navigation}) => (
        { 
        title: 'Perfil',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#666A86',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}

const Tab = createMaterialBottomTabNavigator();
function HomeTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="white"
      barStyle={{ backgroundColor: '#666A86' }}  >
      <Tab.Screen name="Home" component={HomeStackScreen}
                  options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color }) => (
                      <MaterialCommunityIcons name="home" color={color} size={26} />
                    ),
                  }} />
      <Tab.Screen name="Restauntes" component={RestaurantStackScreen}
                     options={{
                    tabBarLabel: 'Restaurantes',
                    tabBarIcon: ({ color }) => (
                      <MaterialCommunityIcons name="food" color={color} size={26} />
                    ),
                  }} />
      <Tab.Screen name="Perfil" component={perfilStackScreen}
                  options={{
                    tabBarLabel: 'Mi Perfil',
                    tabBarIcon: ({ color }) => (
                      <MaterialCommunityIcons name="account-circle" color={color} size={26} />
                    ),
                  }} />
    </Tab.Navigator>
  );
}

function MainStackNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Login'
        screenOptions={{
          headerStyle: {
            backgroundColor: '#101010'
          },
          headerTitleStyle: {
            fontWeight: 'bold'
          },
          headerTintColor: '#ffffff',
          headerBackTitleVisible: false
        }}
        headerMode='float' >
        <Stack.Screen
          name='Login'
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='Registrarse'
          component={Registrarse}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='Home'
          component={HomeTabs}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default MainStackNavigator